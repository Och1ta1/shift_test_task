FROM python:3.11.3

RUN pip install poetry

COPY ./app /app

RUN poetry config virtualenvs.create false && \
    poetry install --no-root --no-dev

WORKDIR /app

EXPOSE 80

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]
