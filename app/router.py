from fastapi import HTTPException, Depends, status, APIRouter
from datetime import datetime, timedelta
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.session import Session

from app.database import get_db
from app.models import Employee
from app.schemas import SalaryResponse, EmployeeRegistration

router = APIRouter()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/token")


def generate_token(username: str, password: str, db_session: Session) -> str:
    employee = db_session.query(Employee).filter(
        Employee.username == username,
        Employee.password == password
    ).first()

    if employee:
        expiration_time = datetime.utcnow() + timedelta(weeks=1)
        token = f"{username}:{expiration_time.timestamp()}"
        return token

    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid credentials"
        )


@router.post("/token")
async def login_for_access_token(
    form_data: OAuth2PasswordRequestForm = Depends(),
    db: Session = Depends(get_db)
):
    token = generate_token(form_data.username, form_data.password, db)
    return {"access_token": token, "token_type": "bearer"}


@router.get("/salary", response_model=SalaryResponse)
async def get_salary(
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(get_db)
):
    username, expiration_timestamp = token.split(":")
    expiration_time = datetime.utcfromtimestamp(float(expiration_timestamp))

    if datetime.utcnow() > expiration_time:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Token expired"
        )

    else:
        employee = db.query(Employee).filter(
            Employee.username == username
        ).first()

        if not employee:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Employee not found"
            )
        return {
            "salary": employee.salary,
            "next_raise_date": str(employee.next_raise_date)
        }


@router.post("/register")
async def register_employee(
    employee: EmployeeRegistration,
    db: Session = Depends(get_db)
):

    try:
        next_raise_date = datetime.strptime(
            employee.next_raise_date,
            "%Y-%m-%d"
        )
        db_employee = db.query(Employee).filter(
            Employee.username == employee.username
        ).first()

        if db_employee:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Username already exists"
            )
        new_employee = Employee(
            username=employee.username, password=employee.password,
            salary=employee.salary, next_raise_date=next_raise_date
        )
        db.add(new_employee)
        db.commit()
        return {"message": "Employee registered successfully"}

    except IntegrityError:
        db.rollback()
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Username already exists"
        )
