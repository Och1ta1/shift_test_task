from pydantic import BaseModel


class TokenRequest(BaseModel):
    username: str
    password: str


class SalaryResponse(BaseModel):
    salary: float
    next_raise_date: str


class EmployeeRegistration(BaseModel):
    username: str
    password: str
    salary: float
    next_raise_date: str
