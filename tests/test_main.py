import pytest
from fastapi.testclient import TestClient
from sqlalchemy.orm import sessionmaker

from app.models import Base
from app.router import generate_token
from main import app
from app.database import engine


@pytest.fixture
def client():
    with TestClient(app) as client:
        yield client


@pytest.fixture(scope="module")
def db():
    Base.metadata.create_all(bind=engine)
    TestingSessionLocal = sessionmaker(
        autocommit=False,
        autoflush=False,
        bind=engine
    )
    db = TestingSessionLocal()
    yield db
    db.close()
    Base.metadata.drop_all(bind=engine)


def test_register_employee(client, db):
    new_employee = {
        "username": "test_user",
        "password": "test_password",
        "salary": 60000.0,
        "next_raise_date": "2024-05-10"
    }
    response = client.post("/register", json=new_employee)
    assert response.status_code == 200
    assert response.json() == {
        "message": "Employee registered successfully"
    }


def test_create_token(client, db):
    response = client.post(
        "/token",
        data={"username": "test_user", "password": "test_password"}
    )
    assert response.status_code == 200
    assert "access_token" in response.json()


def test_get_salary(client, db):
    token = generate_token("test_user", "test_password", db)
    headers = {"Authorization": f"Bearer {token}"}
    response = client.get("/salary", headers=headers)
    assert response.status_code == 200


