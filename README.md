## Описание проекта

Представленный проект реализован в рамках тестового задания.

### Задание:
Реализуйте REST-сервис просмотра текущей зарплаты и даты следующего
повышения. Из-за того, что такие данные очень важны и критичны, каждый
сотрудник может видеть только свою сумму. Для обеспечения безопасности, вам
потребуется реализовать метод где по логину и паролю сотрудника будет выдан
секретный токен, который действует в течение определенного времени. Запрос
данных о зарплате должен выдаваться только при предъявлении валидного токена.

### Требования к решению

Обязательные технические требования:
- код размещен и доступен в публичном репозитории на GitLab;
- оформлена инструкция по запуску сервиса и взаимодействию с проектом
(Markdown файл с использованием конструкций разметки от GitLab по
необходимости);
- сервис реализован на FastAPI.

Необязательные технические требования (по возрастанию трудоемкости):
- зависимости зафиксированы менеджером зависимостей poetry;
- написаны тесты с использованием pytest;
- реализована возможность собирать и запускать контейнер с сервисом в Docker.

## Инструкция по установке и использованию API

##### Клонируйте репозиторий в заранее созданную папку
```bash
cd <folder_name>
git clone https://gitlab.com/Och1ta1/shift_test_task
``` 

### Установка зависимостей

Установить зависимости с помощью:
- pip
    ```bash
    pip install -r .\requirements.txt
    ```
- poetry  
  Установить poetry с помощью pip
  ```bash
  pip install poetry
  ```
  Установить зависимости
  ```bash
  poetry install
  ```


<details><summary>Запуск с помощью Dockerfile</summary>
Затем, чтобы создать Docker-образ, выполните команду:  

```bash 
docker build -t my_fastapi_app .
```  

Где my_fastapi_app - это имя вашего Docker-образа.  
После успешного создания образа вы можете запустить контейнер с помощью команды:  

```bash 
docker run -d --name my_fastapi_container -p 80:80 my_fastapi_app
```
</details>  

<details><summary>Запуск через docker-compose</summary>
Выполнить команду 

```bash
docker-compose up
```
Посмотреть логи
```bash
docker-compose logs
```
Для остановки
```bash
docker-compose down
```
</details>


<details><summary>Запуск через Makefile</summary>
Посмотреть доступные командыДля просмотра всех доступных команд в Makefile выполните следующую команду:

```bash
make help
```
Компиляция проекта.  
Для компиляции проекта выполните следующую команду:

```bash
make build
```
Это создаст исполняемый файл или скомпилирует ваш проект, в зависимости от его типа.  

Запуск приложения.  
Для запуска вашего приложения выполните следующую команду:
```bash
make run
```
Это запустит ваше приложение.

Тестирование. Для запуска тестов выполните следующую команду:  
```bash
make test
````
Это выполнит все тесты в вашем проекте.

Очистка. Для удаления временных файлов или очистки проекта выполните следующую команду:
```bash
make clean
````
Это удалит все временные файлы, созданные в процессе компиляции и тестирования.

</details>


<details><summary>Для локального запуска</summary>

Установить виртуальное окружение env

```bash
python -m venv venv
```
Запустить виртуальный сервер с помощью Uvicorn
```bash
uvicorn main:app --reload
```
</details>

## Проверка работы

### JSON-запросы для Postman

#### Регистрация нового сотрудника:  
URL: POST ```http://127.0.0.1:8000/register```  
Body (формат JSON):  
```
{
    "username": "your_username",
    "password": "your_password",
    "salary": 50000.0,
    "next_raise_date": "2024-05-10"
}
```

#### Аутентификация и получение токена:
URL: POST ```http://127.0.0.1:8000/token```  
username: ваше_имя_пользователя  
password: ваш_пароль  
Пример JSON-запроса:  

``` 
{
    "username": "your_username",
    "password": "your_password"
}
```
#### Получение информации о зарплате:
URL: GET ```http://127.0.0.1:8000/salary```  
Authorization: Bearer, например: (Работник:1715247697.030923)   
Пример JSON-запроса: отсутствует, так как запрос GET и не требует тела запроса.  


#### В проекте присутствует тестирование всех routers(/register, /token, /salary)

_Чтобы запустить тестирование, напишите в консоль: 
```bash 
pytest
```

### Проект выполнил [Кабанов Антон](https://github.com/Och1ta)
